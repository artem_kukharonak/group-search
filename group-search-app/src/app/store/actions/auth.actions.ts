import { createAction } from '@ngrx/store';

export const initUserAction = createAction('[Auth Component] User Initial');

export function initUser(user: string) {
    return { type: initUserAction.type, user };
}
