import { initUserAction } from '../actions/auth.actions';

export const initialState = {
  user: '',
};

export function userReducer(state = initialState, action: any) {
  switch (action.type) {
    case initUserAction.type:
      return { ...state, user: action.user };
    default:
      return state;
  }
}
