import firebase from "firebase/app";
import "firebase/database";
import "firebase/auth";

const firebaseConfig = {
  apiKey: "AIzaSyDShsE0RjODpPkeLW4n-ed1bWOy18xoQ4c",
  authDomain: "travel-party.firebaseapp.com",
  projectId: "travel-party",
  storageBucket: "travel-party.appspot.com",
  messagingSenderId: "128054464107",
  appId: "1:128054464107:web:da4ae3416d911d284cab2e"
};

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

export const auth = firebase.auth();
