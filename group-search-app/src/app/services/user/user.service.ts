import { Location } from '@angular/common';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { initUser } from 'src/app/store/actions/auth.actions';
import { auth } from '../firebase/firebase';

interface RegisterUser {
  name: string,
  surname: string
  age: number,
  email: string,
  password: string,
}

@Injectable({
  providedIn: 'root'
})
export class UserService {

  public baseURL: string = '/';
  public enterURL: string = '/enter';
  public registerURL: string = '/register';
  public recoveryURL: string = '/password-recovery'

  constructor(
    private store: Store<any>,
    private _router: Router,
    private _location: Location) { }

  public getUser(): void {
    auth.onAuthStateChanged((user: any) => user ? this.store.dispatch(initUser(user.uid)) : '');
  }

  public exitUser(): void {
    auth.signOut()
      .then(_ => this.relocationUser(this.enterURL))
      .catch((err: Error) => console.error(err));
  }

  public enterUser(user: any): void {
    auth.signInWithEmailAndPassword(user.email, user.password)
      .then(_ => this.relocationUser(this.baseURL))
      .catch((err: Error) => alert(err.message));
  }

  public registerUser(user: RegisterUser): void {
    auth.createUserWithEmailAndPassword(user.email, user.password)
      .catch((err: Error) => alert(err.message));
  }

  public relocationUser(url: string): void {
    this._router.navigateByUrl(url)
      .catch((err: Error) => console.error(err));
  }

  public relocationBack(): void {
    this._location.back();
  }
}
