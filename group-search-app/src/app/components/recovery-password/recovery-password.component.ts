import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user/user.service';

@Component({
  selector: 'app-recovery-password',
  templateUrl: './recovery-password.component.html',
  styleUrls: ['./recovery-password.component.scss']
})
export class RecoveryPasswordComponent implements OnInit {

  constructor(private userService: UserService) { }

  ngOnInit(): void {
  }

  public handleBackRelocation() {
    this.userService.relocationBack();
  }

}
