import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { UserService } from 'src/app/services/user/user.service';

interface User {
  user: string;
}

interface UserData {
  email?: string;
  password?: string;
}

@UntilDestroy()
@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit, OnDestroy {

  public user$: Observable<any> = this.store.select('user');
  public user: string = '';

  @Input() public enterEmail: string = '';
  @Input() public enterPassword: string = '';

  public enterEmailControl: FormControl = new FormControl();
  public enterPasswordControl: FormControl = new FormControl();

  constructor(
    private store: Store<any>,
    private userService: UserService) { }

  public ngOnInit(): void {
    this.handleInitUser();
    this.handleEnterPassword();
    this.handleEnterEmail();
  }

  public ngOnDestroy(): void { }

  public handleInitUser(): void {
    this.userService.getUser();
    this.user$
      .pipe(untilDestroyed(this))
      .subscribe((user: User) => {
        this.user = user.user;
        this.handleRelocationUser();
      });
  }

  private handleEnterEmail(): void {
    this.enterEmailControl.valueChanges
      .pipe(untilDestroyed(this))
      .subscribe(email => {
        this.enterEmail = email;
        console.log(this.enterEmail);
      });
  }

  private handleEnterPassword(): void {
    this.enterPasswordControl.valueChanges
      .pipe(untilDestroyed(this))
      .subscribe(password => {
        this.enterPassword = password;
        console.log(this.enterPassword);
      });
  }

  public handleRelocationRegister(): void {
    this.userService.relocationUser(this.userService.registerURL);
  }

  public handleRelocationRecovery(): void {
    this.userService.relocationUser(this.userService.recoveryURL);
  }

  public handleEnterUser(): void {
    this.userService.enterUser({
      email: this.enterEmail,
      password: this.enterPassword,
    });
  }

  public handleRelocationUser(): void {
    if (this.user)
      this.userService.relocationUser(this.userService.baseURL);
  }

}
