import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { UserService } from 'src/app/services/user/user.service';

interface RegisterUser {
  name: string,
  surname: string
  age: number,
  email: string,
  password: string,
}

@UntilDestroy()
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit, OnDestroy {

  private registerUser: RegisterUser = {
    name: '',
    surname: '',
    age: 0,
    email: '',
    password: '',
  };

  public registerForm: FormGroup = new FormGroup({
    registerName: new FormControl(),
    registerSurname: new FormControl(),
    registerAge: new FormControl(),
    registerEmail: new FormControl(),
    registerPassword: new FormControl(),
  });

  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.handleRegisterData();
  }

  ngOnDestroy(): void { }

  private handleRegisterData(): void {
    this.registerForm.valueChanges
      .pipe(untilDestroyed(this))
      .subscribe(data => this.registerUser = {
        name: data.registerName,
        surname: data.registerSurname,
        age: data.registerAge,
        email: data.registerEmail,
        password: data.registerPassword,
      })
  }

  public handleRegisterUser(): void {
    this.userService.registerUser(this.registerUser);
    this.userService.relocationUser(this.userService.baseURL);
  }

  public handleBackRelocation() {
    this.userService.relocationBack();
  }

}
