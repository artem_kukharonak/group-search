import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user/user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public title = 'Travel Party';

  constructor(private userService: UserService) { }

  public ngOnInit(): void {
  }

  public handleExitUser(): void {
    this.userService.exitUser();
  }

}
